﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//  FOR UNITY ANALYTICS
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    //
    public int totalMatches;
    public int wrongMatches;

    public int wrongStreakTally = 0;
    public int correctStreakTally = 0;

    public float totalTime;
    //

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    public void Update()
    {
        totalTime += Time.deltaTime;
    }

    //  FROM: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/modulus-operator
    //  % operator computes the remainder after dividing its first operand by its second
    //  ie. Console.WriteLine( 5 % 2 ) will give the output of "1" (5/2 = 2 remainder 1)

    //  Loads the level (as opposed to having multiple scenes with single levels)
    //  It then obtains a list of symbols that are required for that specific level
    private void LoadLevel( int levelNumber )
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //  Instantiating the tiles needed for the level; places each tile a certain offset apart (spacing)
        ///////////////////////////////
        //  Removes the symbol from the index
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                //  gets texture of tiles (which is one single texture?)
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );   //Initialize (Tile.cs)
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    //  Assigning cards a symbol as a list
    //  int cardTotal = level.Rows * level.Columns;
    //  ^ Calculates the total number of cards in the current level by using the # of cards in the row * # of cards in column
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //  Enum = collection of related constants
        //  
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )    //if remaining cards is greater than 0/positive
            //  Total # of cards / 2 will check to see if there's an even number of cards
            //  If there is a remainder (ie. there isn't an even number), print "There must be..."
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //  If there are no symbols assigned (through Inspector), print "The level has no symbols set"
        //  If the number of symbols assigned is greater than half of the total number of cards, print "There are too many symbols..."
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //  Assigns the number of different types of symbols needed, in accordance to the number of cards in the level
        //  Because this is a match-2 game, there must at least be 2 symbols for a pair
        {
            //  hypothetical scenario:
            //  3 different kinds of symbols, each one shows up twice
            //  10 cards; 4 of these 10 cards will have repeated symbols (ones that have shown up already)
            //  ie. 2 stars, 4 squares, 4 triangles

            //  Gets the number of repeats required
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                //  List of symbols + list of duplicated list of symbols
                List<Symbols> symbolsCopy = new List<Symbols>( symbols ); 
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )   //  loop for as many duplicates as needed for level
                {                                                                       //  
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count ); //  every game the symbol that is duplicated is RANDOMIZED
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //  Called earlier with texture
    //  ileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
    //  Splitting the material apart to get individual symbols from the material
    //  and then assigning them to the array of symbols (?)

    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //  Scales the camera size in accordance to the number of tiles in the level
    //  The more tiles, the more the camera must fit and therefor zoom out
    //  *Orthographic size: scales based off of the number of tile rows; adds space equal to (# of rows * tileSpacing) to the # of rows
    //  *transform.position: moves the camera in accordance to the number of columns; 
    //                      also  adds space equal to (# of rows * tileSpacing) to the # of rows
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //  First selection is tileOne
    //  Reveal() (in Tile.cs) flips the tile around, revealing its symbol
    //
    //  Second selection (if its not the first tile, and tileTwo doesn't exist)
    //  Reveal()
    //
    //  If tileOne and tileTwo match; "hide" them (destroys them)
    //  otherwise (if they don't match), Hide() (which flips them back around)
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
                totalMatches++;

                //wrongStreakTally logic:
                //Every time player gets a wrong match, +1 to tally
                //Analytics would get this number
                //And once the player breaks the streak by getting a correct match,
                //the wrongStreakTally would reset.

                //adds 1 to the correct tally, keeping a streak
                correctStreakTally++;

                if (wrongStreakTally > 0)
                {
                    wrongStreakTally = 0;
                }

                //Dictionary<string, object> incorrectStreakParameters = new Dictionary<string, object>();
                //incorrectStreakParameters["IncorrectStreak"] = wrongStreakTally;
                //Analytics.CustomEvent("IncorrectStreakCount", incorrectStreakParameters);

                Dictionary<string, object> correctStreakParameters = new Dictionary<string, object>();
                correctStreakParameters["CorrectStreak"] = correctStreakTally;
                Analytics.CustomEvent("CorrectStreakCount", correctStreakParameters);
            }
            else
            {
                //wrong match

                StartCoroutine( WaitForHide( false, 1f ) );
                totalMatches++;
                wrongMatches++;

                //adds 1 to the wrong tally, keeping a streak
                wrongStreakTally++;

                if(correctStreakTally > 0)
                {
                    correctStreakTally = 0;
                }

                //Dictionary<string, object> correctStreakParameters = new Dictionary<string, object>();
                //correctStreakParameters["CorrectStreak"] = correctStreakTally;
                //Analytics.CustomEvent("CorrectStreakCount", correctStreakParameters);

                Dictionary<string, object> incorrectStreakParameters = new Dictionary<string, object>();
                incorrectStreakParameters["IncorrectStreak"] = wrongStreakTally;
                Analytics.CustomEvent("IncorrectStreakCount", incorrectStreakParameters);
            }
        }
    }

    //  Level completed, adds 1 to the CurrentLevelNumber which proceeds game to next level
    //  If CurrentLevelNumber is greater than total number of levels (Levels.length - 1, since it starts on 0),
    //  Print "GameOver"
    //  
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }

        // tracking number of pairs created
        Dictionary<string, object> clickParameters = new Dictionary<string, object>();
        clickParameters["PairsMade"] = totalMatches;    // total amount of pairs made during level
        clickParameters["IncorrectPairs"] = wrongMatches;   //  total amount of wrong pairs 
        clickParameters["CorrectPairs"] = totalMatches - wrongMatches;  //total amount of correct pairs
        Analytics.CustomEvent("Matches per level", clickParameters);

        //  tracking time it took to finish this level
        Dictionary<string, object> levelParameters = new Dictionary<string, object>();
        levelParameters["LevelCompletionTime"] = Time.timeSinceLevelLoad;
        Analytics.CustomEvent("Matches per level", clickParameters);

    }

    ///////  
    //  If tiles match, destroy both, and subtract 2 from the total cards remaining
    //  If tiles do not match, Hide() both
    //  Deselects both tiles (with null)
    //  If there are no more tiles remaining, the level is complete (LevelComplete())
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }

    //  when the player leaves the game
    private void OnApplicationQuit()
    {
        Dictionary<string, object> endParameters = new Dictionary<string, object>();
        //endParameters["PlayTime"] = totalTime + " seconds";    // total time the player has played for (entirety of game)
        endParameters["PlayTime"] = Time.time + " seconds";
        endParameters["LastLevel"] = CurrentLevelNumber + 1;    //ended level
        Analytics.CustomEvent("TotalPlayTime", endParameters);

    }
}
