﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //  Gets the child of this game object (Card clone (parent) -> Blank Card (child))
    //  FROM: https://docs.unity3d.com/ScriptReference/Transform.GetChild.html
    //  "Assigns the first child of the game object" ( 0 )

    //  Gets the collider of the game object (Card clone)
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //  This script is attached to each individual tile.
    //  When this game object is clicked, it'll run the TileSelected function (found in GameManager).
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //  FROM: https://docs.unity3d.com/Manual/UnityIAPIStoreInitialization.html
    //  FROM: https://docs.unity3d.com/Manual/UnityIAP.html
    //
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //  When selected, this tile will Reveal itself; runs coroutine
    //  FROM: https://docs.unity3d.com/Manual/Coroutines.html
    //  "...a function that has the ability to pause execution and return control to Unity but then to
    //  continue where it left off in the following frame."
    //  Allows actions/functions to run alongside eachother (?)
    //  Are coroutines like traffic conductors????

    //  Card is rotated 180 degrees, revealing its "backside"
    //  Collider is also disabled; cannot click it after it's already been clicked
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //  Spins the card back to hide its content (target 0 degrees)
    //  Re-enables collider so it can be clicked again
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //  FROM: http://answers.unity3d.com/questions/1004821/what-are-ienumerator-and-coroutine.html
    /////  ????????
    //  FROM: https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
    //  float target is in angles; time is clamped to range [0, 1]
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
